package pathfinding

import (
	"runtime"
	"testing"
)

func TestPathfinding(t *testing.T) {
	m := NewMap(5)
	start := m.node(0, 0)
	goal := m.node(m.size-1, m.size-1)

	m.node(2, 1).walkable = false
	m.node(2, 2).walkable = false
	m.node(2, 3).walkable = false

	path := m.FindPath(start, goal)

	if len(path) != 6 {
		t.Error("sub-optimal path found")
	}
	if path[5].x != 4 && path[5].y != 4 {
		t.Error("goal not reached")
	}
}

var (
	mapSize = 100
	agents  = 100
)

func BenchmarkOnePath(b *testing.B) {
	m := NewMap(mapSize)
	for x := 0; x < b.N; x++ {
		start := m.node(0, 0)
		goal := m.node(m.size-1, m.size-1)
		m.FindPath(start, goal)
	}
}
func BenchmarkPathfindingNoGoRoutines(b *testing.B) {
	m := NewMap(mapSize)
	for x := 0; x < b.N; x++ {
		for i := 0; i < agents; i++ {
			start := m.node(0, 0)
			goal := m.node(m.size-1, m.size-1)
			m.FindPath(start, goal)
		}
	}
}
func BenchmarkPathfindingOneMaxProc(b *testing.B) {
	runtime.GOMAXPROCS(1)
	m := NewMap(mapSize)
	done := make(chan bool)

	for x := 0; x < b.N; x++ {
		for i := 0; i < agents; i++ {
			go func(done chan bool) {
				start := m.node(0, 0)
				goal := m.node(m.size-1, m.size-1)
				m.FindPath(start, goal)
				done <- true
			}(done)
		}
		<-done
	}
}
func BenchmarkPathfindingNumCPUMaxProcs(b *testing.B) {
	runtime.GOMAXPROCS(runtime.NumCPU())
	m := NewMap(mapSize)
	done := make(chan bool)

	for x := 0; x < b.N; x++ {
		for i := 0; i < agents; i++ {
			go func(done chan bool) {
				start := m.node(0, 0)
				goal := m.node(m.size-1, m.size-1)
				m.FindPath(start, goal)
				done <- true
			}(done)
		}
		<-done
	}
}
