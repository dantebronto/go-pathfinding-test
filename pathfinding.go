package pathfinding

import (
	"container/heap"
)

type Vector2 struct {
	x, y int
}

type Node struct {
	Vector2
	walkable bool

	parent  *Node
	closed  bool
	visited bool
	f, g, h int

	index int
}

type Map struct {
	size   int
	matrix [][]Node
}

func (m *Map) node(x, y int) *Node {
	return &m.matrix[x][y]
}

func (m *Map) neighbors(i, j int) []*Node {
	neibs := []*Node{}

	node := m.node(i, j)

	x := node.x
	y := node.y

	surrounds := [][]int{
		{x - 1, y - 1}, {x, y - 1}, {x + 1, y - 1},
		{x - 1, y}, {x + 1, y},
		{x - 1, y + 1}, {x, y + 1}, {x + 1, y + 1},
	}

	for _, coords := range surrounds {
		if coords[0] > -1 && coords[1] > -1 && coords[0] < m.size && coords[1] < m.size {
			neibs = append(neibs, m.node(coords[0], coords[1]))
		}
	}

	return neibs
}

func NewMap(size int) *Map {

	matrix := make([][]Node, size)

	for i, _ := range matrix {
		matrix[i] = make([]Node, size)
	}

	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			node := Node{walkable: true, visited: false}
			node.x = i
			node.y = j
			matrix[i][j] = node
		}
	}

	return &Map{size, matrix}
}

func (oldMap *Map) deepCopy() *Map {
	size := oldMap.size
	newMap := NewMap(size)

	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			oldNode := oldMap.node(i, j)
			newNode := newMap.node(i, j)
			newNode.walkable = oldNode.walkable
		}
	}

	return newMap
}

func buildPath(node *Node) (path []Vector2) {
	var nodePath []*Node

	curr := node

	for curr.parent != nil {
		nodePath = append(nodePath, curr)
		curr = curr.parent
	}

	// reverse the generated path
	for i := len(nodePath) - 1; i > -1; i-- {
		path = append(path, Vector2{nodePath[i].x, nodePath[i].y})
	}

	return path
}

func (oldMap *Map) FindPath(start *Node, goal *Node) []Vector2 {
	m := oldMap.deepCopy()
	var openQ PriorityQueue
	var currentNode *Node

	openQ.Push(start)

	for openQ.Len() > 0 {
		currentNode = openQ.Pop().(*Node)

		if currentNode.x == goal.x && currentNode.y == goal.y {
			return buildPath(currentNode)
		}

		currentNode.closed = true

		for _, neighbor := range m.neighbors(currentNode.x, currentNode.y) {
			tentativeGScore := currentNode.g + 1
			if !neighbor.closed && neighbor.walkable {
				beenVisited := neighbor.visited
				if !beenVisited || tentativeGScore < neighbor.g {
					neighbor.visited = true
					neighbor.parent = currentNode
					neighbor.h = manhattanDist(neighbor.x, neighbor.y, goal.x, goal.y)
					neighbor.g = tentativeGScore
					neighbor.f = neighbor.g + neighbor.h
					if !beenVisited {
						openQ.Push(neighbor)
					}
				}
			}
		}
	}
	// no path found
	return nil
}

func manhattanDist(x1, y1, x2, y2 int) int {
	t1 := x2 - x1
	t2 := y2 - y1
	if t1 < 0 {
		t1 = -t1
	}
	if t2 < 0 {
		t2 = -t2
	}
	return t1 + t2
}

// Priority queue implementation

// A PriorityQueue implements heap.Interface and holds Nodes.
type PriorityQueue []*Node

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	// We want Pop to give us the highest, not lowest, priority (f value) so we use greater than here.
	return pq[i].f > pq[j].f
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue) Push(x interface{}) {
	n := len(*pq)
	item := x.(*Node)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	item.index = -1 // for safety
	*pq = old[0 : n-1]
	return item
}

// update modifies the f value (priority) of a Node in the queue.
func (pq *PriorityQueue) update(item *Node, priority int) {
	heap.Remove(pq, item.index)
	item.f = priority
	heap.Push(pq, item)
}
